import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Main from './Main';
import Header from './Header';
import ItemDetail from './components/ItemDetail';

const App = () => {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route path="/details" component={ItemDetail} />
        <Route exact path="/" component={Main} />
      </Switch>
    </div>
  );
};

export default App;
