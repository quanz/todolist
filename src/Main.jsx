import React from 'react';

import TaskForm from './components/TaskForm';
import TaskList from './components/TaskList';

const Main = () => {
  return (
    <div>
      <TaskForm />
      <TaskList />
    </div>
  );
};

export default Main;
