import { ADD_ITEM, DELETE_ITEM, UPDATE_ITEM, SHOW_ITEM, IS_EDITING } from './constants';

const initialState = {
  totalId: '0',
  tasks: [],
  taskId: null,
  isEditing: false
};

function todoListReducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_ITEM:
      return {
        ...state,
        tasks: [...state.tasks, action.payload],
        totalId: action.payload.id
      };
    case DELETE_ITEM:
      return {
        ...state,
        tasks: [...state.tasks.filter(task => task.id !== action.payload)]
      };
    case SHOW_ITEM:
      return {
        ...state,
        taskId: action.payload
      };
    case UPDATE_ITEM:
      return {
        ...state,
        tasks: state.tasks.map(task => {
          if (task.id === action.payload.id) {
            return { ...task, nickname: action.payload.nickname };
          }
          return task;
        }),
        isEditing: !state.isEditing
      };
    case IS_EDITING:
      return {
        ...state,
        isEditing: true
      };
    default:
      return state;
  }
}

export default todoListReducer;
