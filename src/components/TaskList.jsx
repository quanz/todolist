import React from 'react';
import { connect } from 'react-redux';

import { deleteTask, showTask } from '../redux/action';
import Item from './Item';

const TaskList = ({ tasks, ondeleteTask, onShowTask }) => {
  return (
    <div>
      <h3 className="post_heading">Task List</h3>
      {tasks.map(task => (
        <Item key={task.id} task={task} ondeleteTask={ondeleteTask} onShowTask={onShowTask} />
      ))}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    tasks: state.tasks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ondeleteTask: id => dispatch(deleteTask(id)),
    onShowTask: id => dispatch(showTask(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskList);
