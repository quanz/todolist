import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addTask } from '../redux/action';

class TaskForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    const { totalId, onAddTask } = this.props;
    const name = this.getName.value;
    const nickname = this.getNickname.value;
    const data = {
      id: (parseInt(totalId, 10) + 1).toString(),
      name,
      creationTime: new Date(),
      nickname: { data: { name: nickname } }
    };
    onAddTask(data);
    this.getName.value = '';
    this.getNickname.value = '';
  };

  render() {
    return (
      <div className="post-container">
        <h3 className="post_heading">Add Task</h3>
        <form className="form" onSubmit={this.handleSubmit}>
          <input
            required
            type="text"
            ref={input => {
              this.getName = input;
            }}
            placeholder="Add Task Name"
          />
          <br />
          <br />
          <textarea
            // required
            rows="3"
            ref={input => {
              this.getNickname = input;
            }}
            cols="18"
            placeholder="Task nickname"
          />
          <br />
          <button type="submit">Add</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { totalId } = state;
  return {
    totalId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAddTask: task => dispatch(addTask(task))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskForm);
