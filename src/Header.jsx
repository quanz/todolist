import React from 'react';

const Header = () => {
  return (
    <div className="navbar">
      <h2 className="center">TODO LIST</h2>
    </div>
  );
};

export default Header;
