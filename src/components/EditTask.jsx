import React, { Component } from 'react';
import { connect } from 'react-redux';

import { editTask } from '../redux/action';

class EditTask extends Component {
  handleEdit = e => {
    e.preventDefault();
    const { id, onEditTask } = this.props;
    const newNickname = this.getNickname.value;
    const data = {
      id,
      nickname: { data: { name: newNickname } }
    };
    onEditTask(data);
  };

  render() {
    const { nickname } = this.props;
    return (
      <div className="post">
        <form className="form" onSubmit={this.handleEdit}>
          <textarea
            required
            rows="3"
            ref={input => {
              this.getNickname = input;
            }}
            defaultValue={nickname}
            cols="18"
            placeholder="Enter New Nickname"
          />
          <br />
          <br />
          <button type="submit">Save</button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onEditTask: newName => dispatch(editTask(newName))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(EditTask);
