import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';

import { isEditTask } from '../redux/action';
import EditTask from './EditTask';

const ItemDetail = ({ taskId, tasks, isEditing, onIsEditingTask }) => {
  const taskDetail = tasks.filter(task => task.id === taskId)[0];
  /**
   * Handle Error
   */
  if (!taskDetail)
    return (
      <div className="form">
        <p className="post_message">No Task Details. Please go back to Home Page</p>
        <button type="button">
          <Link to="/" style={{ textDecoration: 'none', color: 'white' }}>
            Back
          </Link>
        </button>
      </div>
    );

  /**
   * Show Task details and edit nickname
   */
  const { id, name, creationTime, nickname } = taskDetail;
  return (
    <div className="post-container">
      <h1 className="post_heading">Task Details</h1>
      <div className="detail_wrapper">
        <h4 className="post_detail">Task ID: </h4>
        <p className="info_detail">{id}</p>
        <h4 className="post_detail">Task Name: </h4>
        <p className="info_detail">{name}</p>
        <h4 className="post_detail">Create Date: </h4>
        <p className="info_detail">{creationTime.toDateString()}</p>
        <h4 className="post_detail">Task Nickname:</h4>
        {isEditing ? (
          <EditTask id={id} nickname={nickname.data.name} />
        ) : (
          <p className="info_detail">{nickname.data.name}</p>
        )}
        <div className="control-buttons">
          <button type="button" className="edit" onClick={onIsEditingTask}>
            Edit
          </button>
          <button type="button" className="delete">
            <Link to="/" style={{ textDecoration: 'none', color: 'white' }}>
              Back
            </Link>
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  const { tasks, taskId, isEditing } = state;
  return {
    tasks,
    taskId,
    isEditing
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onIsEditingTask: () => dispatch(isEditTask())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ItemDetail)
);
