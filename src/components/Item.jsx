import React from 'react';
import { Link } from 'react-router-dom';

const Item = props => {
  const { task, ondeleteTask, onShowTask } = props;
  return (
    <div className="post">
      <h2 className="post_title">{task.name}</h2>
      <p className="post_message">{task.nickname.data.name}</p>
      <div className="control-buttons">
        <button type="button" className="edit" onClick={() => onShowTask(task.id)}>
          <Link to="/details" style={{ textDecoration: 'none', color: 'white' }}>
            Details
          </Link>
        </button>
        <button type="button" className="delete" onClick={() => ondeleteTask(task.id)}>
          Delete
        </button>
      </div>
    </div>
  );
};

export default Item;
