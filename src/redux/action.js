import { ADD_ITEM, DELETE_ITEM, UPDATE_ITEM, SHOW_ITEM, IS_EDITING } from './constants';

const addTask = task => ({
  type: ADD_ITEM,
  payload: task
});

const deleteTask = id => ({
  type: DELETE_ITEM,
  payload: id
});

const showTask = id => ({
  type: SHOW_ITEM,
  payload: id
});

const editTask = ({ id, nickname }) => ({
  type: UPDATE_ITEM,
  payload: { id, nickname }
});

const isEditTask = () => ({
  type: IS_EDITING
});

export { addTask, deleteTask, showTask, editTask, isEditTask };
